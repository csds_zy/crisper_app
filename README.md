# Wrapper for CRiSPER ArcGIS App

Mobile App for the Crisper tool

See https://survivor.togaware.com/gnulinux/git-developer-workflow.html
for developer github workflow.

## Flutter

If this works it is all we need.

Basically:

```dart
      body: WebView(
        initialUrl: 'https://mobile.crisper.net.au',
        javascriptMode: JavascriptMode.unrestricted,
      )
```

Note, without JavaScript enabled the ArcGIS app sits with the busy
logo.

## Instructions to Sideload onto Android

Phone needs Developer Options to be enabled. To do so, under Settings,
go to *About phone* and the tap *Build number* 7 times. Then under
*Settings* select *Developer options* and the enable *USB debugging*.

```bash
$ adb ...
```

## Instructions to Sideload onto iPhone

Is this even possible?


## Add to TestFlight

Can this be done without creating an Apple account?

https://developer.apple.com/testflight/

## Add to Apple App Store

This needs a $99 per year account?

## Add to FDroid Repository

+ https://gitlab.com/fdroid/fdroiddata/blob/master/CONTRIBUTING.md
+ https://f-droid.org/en/contribute/

## Add to Google Play Store

There is an issue at present on Google Play Store, not allowing COVID
apps unless from Government.

