import 'package:flutter/material.dart';
import 'package:crisper_app/src/webview_container.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: WebViewContainer('https://mobile.crisper.net.au', 'CRISPER'),
    );
  }
}